import java.util.Arrays;
import java.util.Scanner;

public class Main2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
// Объявление переменной считывания данных
        int[] array = new int[46];
// Объявление массива
        for (int i = 0; i < 46; i++) {
            array[i] = scanner.nextInt();
        }
// Цикл
        System.out.println(Arrays.toString(array));
    }
// Вывод данных
}